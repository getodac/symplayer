/*
 * Basic Linux Kernel module using GPIO interrupts.
 *
 * Author:
 * 	Interrupt handling part - Stefan Wendler (devnull@kaltpost.de)
 *  Device part :
 *     Copyright (C) 2013, Jack Whitham
 *     Copyright (C) 2009-2010, University of York
 *     Copyright (C) 2004-2006, Advanced Micro Devices, Inc.
 *
 *  Modified by Getodac for CDC Emulator Rx part.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/time.h>
#include <linux/uaccess.h>

#define GPIO_FOR_RX_SIGNAL 17
#define DEV_NAME "cdcrx"
#define BUFFER_SZ 512

/* Last Interrupt timestamp */
static struct timespec lastIrq_time;
static unsigned long packet[BUFFER_SZ];
static int pRead;
static int pWrite;
static int wasOverflow;

/**
There are three different bit identifiers

Start bit: It consists of a 9ms high phase followed by a 4.5ms low phase.
0 bit: A bit with the value 0 is represented by an equal high and low phase. The length is 0.55ms each. So the bit has a total length of 1.1ms.
1-bit: A 1-valued bit represents a 0.55ms high pulse followed by a 1.7ms low phase. The total length is 2.25ms.
A command consists of 4 bytes, whereby in my interpretation the LSB (least significant bit) of each byte is sent first.

1st byte: always CA hex.
2nd byte: always 34 hex.
3rd byte: The command which is evaluated.
4th byte: Again the command with inverted bit.
The picture above shows the bit sequence "01010011 00101100 01111000 10000111". This corresponds to the hexadecimal representation "CA 34 1e e1". 
The radio has informed in this case that the next title should be played.
*/
// Statuses
#define ST_IDLE					        0
#define ST_START_BIT_H  	 	        1
#define ST_START_BIT_L  		        2
#define ST_START_PACKET			        3
#define ST_ERROR    			        4
#define ST_RECEIVING_BITS               5
#define ST_END_PACKET                   6

// Timings (us)
#define TIMING_START_PACKET_HIGH     	9000
#define TIMING_START_PACKET_LO 	        4500
#define TIMING_BIT_HIGH      	        550
#define TIMING_BIT_0			        550
#define TIMING_BIT_1 			        1700

static int debug=0;
module_param(debug, int, 0);


static int lower_tollerance=100;
module_param(lower_tollerance, int, 0);

static int upper_tollerance=150;
module_param(upper_tollerance, int, 0);

#define LOWER_TOLLERANCE                lower_tollerance
#define UPPER_TOLLERANCE                upper_tollerance


// macros
#define IS_TIMING_IN_RANGE(t, x)        (x > (t - LOWER_TOLLERANCE) && x < (t + UPPER_TOLLERANCE))


// status: first byte (LSB) - status, second byte (MSB) - bit counter
static unsigned short _status = ST_IDLE;
static unsigned int rxdata = 0x00000000;


/* Define GPIOs for RX signal */
static struct gpio signals[] = {
    {GPIO_FOR_RX_SIGNAL, GPIOF_IN, "RX Signal"},  // Rx signal
};

/* Later on, the assigned IRQ numbers for the buttons are stored here */
static int rx_irqs[] = {-1};

static short process_status(long ns, bool din, short currentStatus) {
    short status = currentStatus & 0x00FF;
    short bitCt = (currentStatus >> 8) & 0x00FF;
    
    if (debug && status == ST_ERROR) {
        printk(KERN_INFO "WARN: status=ST_ERROR (4), bitct=%d, ns=%ld, din=%d\n", bitCt, ns, din);
    }

    if (din && (status == ST_IDLE || status == ST_ERROR)) {
        return ST_START_BIT_H;
    }

    if ((!din && status == ST_START_BIT_H) && IS_TIMING_IN_RANGE(TIMING_START_PACKET_HIGH, ns)) {
        return ST_START_BIT_L;
    }

    if ((din && status == ST_START_BIT_L) && IS_TIMING_IN_RANGE(TIMING_START_PACKET_LO, ns)) {
        return ST_RECEIVING_BITS;
    }

    if (status == ST_RECEIVING_BITS) {
        //printk(KERN_INFO "RECEIVING_BITS:  status=%d, bitct=%d, ns=%ld, din=%d\n", status, bitCt, ns, din);
        
        if (din) {
            status = bitCt < 31 ? ST_RECEIVING_BITS : ST_END_PACKET;
            if (IS_TIMING_IN_RANGE(TIMING_BIT_0, ns)) {
                // bit 0
                rxdata &= ~(0x01 << bitCt++);
                return ((bitCt << 8) & 0xFF00) | (status & 0x00FF);
            } else if (IS_TIMING_IN_RANGE(TIMING_BIT_1, ns)) {
                // bit 1
                rxdata |= 0x01 << bitCt++;
                return ((bitCt << 8) & 0xFF00) | (status & 0x00FF);
            } else {
                return ST_ERROR;
            }
        } else {
            if (IS_TIMING_IN_RANGE(TIMING_BIT_HIGH, ns)) {
                return ((bitCt << 8) & 0xFF00) | (ST_RECEIVING_BITS & 0x00FF);
            } else {
                return ST_ERROR;
            }
        }
    }

    if (status == ST_END_PACKET && !din) {
        rxdata = 0;
        return ST_IDLE;
    }
    
    return ST_ERROR;
}

static bool validate_rx_data(unsigned int data) {
    // Do we need that?
    return true;
}

/*
 * The interrupt service routine called on every pin status change
 */
static irqreturn_t rx_isr(int irq, void *data) {
    struct timespec current_time;
    struct timespec delta;
    unsigned long ns;
    int rxline;
    bool lineSt;
    
    getnstimeofday(&current_time);
    delta = timespec_sub(current_time, lastIrq_time);
    ns = ((long long)delta.tv_sec * 1000000) + (delta.tv_nsec / 1000);
 
    rxline = gpio_get_value(GPIO_FOR_RX_SIGNAL);
    lineSt = rxline > 0;
    
    if (debug) {
        printk(KERN_INFO "status=%d, bitct=%d, ns=%ld, lineSt=%d\n", _status& 0x00FF, _status >> 8 & 0x00FF, ns, lineSt);
    }
    _status = process_status(ns, lineSt, _status);

    if ((_status & 0x00FF) == ST_END_PACKET && validate_rx_data(rxdata)) {
        packet[pWrite] = rxdata;
        pWrite = (pWrite + 1) & (BUFFER_SZ - 1);
        if (pWrite == pRead) {
            // overflow
            pRead = (pRead + 1) & (BUFFER_SZ - 1);
            if (wasOverflow == 0) {
                printk(KERN_ERR "CDCRX - Buffer Overflow - IRQ will be missed");
                wasOverflow = 1;
            }
        } else {
            wasOverflow = 0;
        }
    }

    getnstimeofday(&lastIrq_time);

    return IRQ_HANDLED;
}


static int cdc_open(struct inode *inode, struct file *file) {
    return nonseekable_open(inode, file);
}

static int cdc_release(struct inode *inode, struct file *file) {
    return 0;
}

static ssize_t cdc_write(struct file *file, const char __user *buf,
                         size_t count, loff_t *pos) {
    return -EINVAL;
}

static ssize_t cdc_read(struct file *file, char __user *buf,
                        size_t count, loff_t *pos) {
    // returns one of the line with the time between two IRQs
    // return 0 : end of reading
    // return >0 : size
    // return -EFAULT : error
    char tmp[256];
    int _count;
    int _error_count;
	unsigned long val;

    _count = 0;
    if (pRead != pWrite) {
		val = packet[pRead];
        sprintf(tmp, "%02X %02X %02X %02X\n", (unsigned int) (val & 0x00FF), (unsigned int) ((val >> 8) & 0x00FF), (unsigned int) ((val >> 16) & 0x00FF), (unsigned int) ((val >> 24) & 0x00FF));
        _count = strlen(tmp);
        _error_count = copy_to_user(buf, tmp, _count + 1);
        if (_error_count != 0) {
            printk(KERN_ERR "CDCRX - Error writing to char device");
            return -EFAULT;
        }
        pRead = (pRead + 1) & (BUFFER_SZ - 1);
    }
    return _count;
}

static struct file_operations cdc_fops = {
    .owner = THIS_MODULE,
    .open = cdc_open,
    .read = cdc_read,
    .write = cdc_write,
    .release = cdc_release,
};

static struct miscdevice cdc_misc_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = DEV_NAME,
    .fops = &cdc_fops,
};

/*
 * Module init function
 */
static int __init cdc_init(void) {
    int ret = 0;
    printk(KERN_INFO "%s\n", __func__);

    // INITIALIZE IRQ TIME AND Queue Management
    getnstimeofday(&lastIrq_time);
    pRead = 0;
    pWrite = 0;
    wasOverflow = 0;

    // register GPIO PIN in use
    ret = gpio_request_array(signals, ARRAY_SIZE(signals));

    if (ret) {
        printk(KERN_ERR "CDCRX - Unable to request GPIOs for RX Signals: %d\n", ret);
        goto fail2;
    }

    // Register IRQ for this GPIO
    ret = gpio_to_irq(signals[0].gpio);
    if (ret < 0) {
        printk(KERN_ERR "CDCRX - Unable to request IRQ: %d\n", ret);
        goto fail2;
    }
    rx_irqs[0] = ret;
    printk(KERN_INFO "CDCRX - Successfully requested RX IRQ # %d\n", rx_irqs[0]);
    ret = request_irq(rx_irqs[0], rx_isr, IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING, "cdc#din", NULL);
    if (ret) {
        printk(KERN_ERR "CDCRX - Unable to request IRQ: %d\n", ret);
        goto fail3;
    }

    // Register a character device for communication with user space
    misc_register(&cdc_misc_device);

    printk(KERN_INFO "%s: debug=%d, lower_tollerance=%d, upper_tollerance=%d\n", __func__, debug, lower_tollerance, upper_tollerance);
 
    return 0;

    // cleanup what has been setup so far
fail3:
    free_irq(rx_irqs[0], NULL);

fail2:
    gpio_free_array(signals, ARRAY_SIZE(signals));
    return ret;
}

/**
 * Module exit function
 */
static void __exit cdc_exit(void) {
    printk(KERN_INFO "%s\n", __func__);

    misc_deregister(&cdc_misc_device);

    // free irqs
    free_irq(rx_irqs[0], NULL);

    // unregister
    gpio_free_array(signals, ARRAY_SIZE(signals));
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Getodac");
MODULE_DESCRIPTION("Linux Kernel Module for VW/Skoda CDC Emulator Rx");

module_init(cdc_init);
module_exit(cdc_exit);