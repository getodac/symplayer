About
---------

This is a Kernel Driver for decoding data (keypress) from Skoda Symphony Car Radio.

This Kernel Driver is based on kernel interuption instead of Software interruption this helps to correctly handle timing with RPI where kernel respond to software interrupt with an incertain delay.

To compile this kernel driver you need to have your RPI environment ready for it. 

This Kernel driver is based on [this](https://bitbucket.org/disk_91-admin/rfrpi/src/625129fecef974059a4f8f4353aa31e1ca48b8c4/rfrpi_src/krfrpi/?at=master) driver.
More information about krfrpi driver you can find on its [author website](http://www.disk91.com/2015/technology/systems/rf433-raspberry-pi-gpio-kernel-driver-for-interrupt-management/).

Then to compile, execute
> make compile

After you can load the kernel by insmod cdcrx.ko.
You should after that have a /dev/cdcrx device.

CD Changer Protocol
-------------------
You can find more information about CD Changer Protocol [here](cdc-protocol.md).

Links
-------
 * https://www.disk91.com/2015/technology/systems/rf433-raspberry-pi-gpio-kernel-driver-for-interrupt-management/
 * [Source code](https://bitbucket.org/disk_91-admin/rfrpi/src/master/) for rf433 kernel driver
 * https://www.blaess.fr/christophe/2014/01/22/gpio-du-raspberry-pi-mesure-de-frequence/
 * [GPIO IRQ driver](https://github.com/josecm/GPIOIRQ-for-RPi)
 * About [linux kernel modules](http://derekmolloy.ie/writing-a-linux-kernel-module-part-1-introduction/)
 * [Programming interrupts in Raspberry PI](http://blogsmayan.blogspot.com/p/programming-interrupts-in-raspberry-pi.html)
 * [Raspbery Pi gpio interrupts in kernel space C](https://morethanuser.blogspot.com/2013/04/raspbery-pi-gpio-interrupts-in-kernel.html)

(c) Getodac
License : GPL
