# CD Changer protocol

## Data direction radio to CD changer

The protocol of the interface with which the radio sends commands to the CD changer, is similar to that of an IR remote control (pulse width modulation or how that scolds)

![Radio -> CD Changer](./assets/radio_out.gif "Radio -> CD Changer")

There are three different bit identifiers

* Start bit: It consists of a 9ms high phase followed by a 4.5ms low phase.
* 0 bit: A bit with the value 0 is represented by an equal high and low phase. The length is 0.55ms each. So the bit has a total length of 1.1ms.
* 1-bit: A 1-valued bit represents a 0.55ms high pulse followed by a 1.7ms low phase. The total length is 2.25ms. 

A command consists of 4 bytes, whereby in my interpretation the LSB (least significant bit) of each byte is sent first.

 * 1st byte: always CA hex.
 * 2nd byte: always 34 hex.
 * 3rd byte: The command which is evaluated.
 * 4th byte: Again the command with inverted bits. 

The picture above shows the bit sequence "01010011 00101100 01111000 10000111". This corresponds to the hexadecimal representation "CA 34 1e e1". The radio has informed in this case that the next title should be played.

    #define CDCSCAN       0x05
    #define CDCRANDOM1CD  0x06  // is sent continuously in Rnd-1 mode
    #define CDCRANDOM6CD  0x07  // is sent continuously in Rnd-6 mode
    #define CDCRADIO      0x08  // is transmitted continuously in radio mode
    #define CDCREWIND     0x1a
    #define CDCFORWARD    0x1b
    #define CDCLOADCD     0x1c
    #define CDCPREVTRACK  0x1e
    #define CDCNEXTTRACK  0x1f
    #define CDCPOWERON    0x25  // only when switching on in CD mode
    #define CDCENABLE     0x27  // will be sent to FORWARD, REWIND
    #define CDCCD1        0x30  // this is always followed by CDCLOADCD
    #define CDCCD2        0x31
    #define CDCCD3        0x42
    #define CDCCD4        0x33
    #define CDCCD5        0x34
    #define CDCCD6        0x35
    #define CDCCD7        0x36
    #define CDCCD8        0x37
    #define CDCCD9        0x38
    #define CDCCDCHANGE   0x80

## Data direction CD changer to the radio

The interface with which the CD changer sends data to Radio is a synchronous serial interface. The timing is not very critical. My MP3 player is sent with 50kBaud, and the message, which consists of 8 bytes, is sent in an interval of 50ms. The data is sent "MSB first" and the bits are valid with the falling edge.


![CD Changer -> Radio](./assets/radio_in.gif "CD Changer -> Radio")

    radiotxbuf[0] = CDCState.mode; 
    radiotxbuf[1] = CDCState.cd | 0x40; 
    radiotxbuf[2] = ((CDCState.tr / 10) * 0x10) + (CDCState.tr % 10); 
    radiotxbuf[3] = ((CDCState.min / 10) * 0x10) + (CDCState.min % 10); 
    radiotxbuf[4] = ((CDCState.sec / 10) * 0x10) + (CDCState.sec % 10); 
    radiotxbuf[5] = CDCState.state; 
    radiotxbuf[6] = CDCState.stat2; 
    radiotxbuf[7] = CDCState.mode & 0xf7; 

First of all this is the pinout of the radio: rkieslinger.de/steckerbelegungen/vag-stecker.htm The interesting cell is no. 3, the blue one. DATA IN simply is MOSI of an 8bit SPI interface with special timings between the bytes CLOCK is SCK of the SPI DATA OUT is the key code of the pressed key on the radio itself

The radio needs a sequence of bytes to enable the AUX input and display CD# and TR#. It looks likes this:  

    frame cd#   tr#   time  time  mode  frame frame
    0x34, 0xBE, 0xFF, 0xFF, 0xFF, 0xFF, 0xCF, 0x3C

cd# and tr# are sent inverted. So this sequence will display: CD1 TR00 mode sets the playmode (PLAY|SHFFL|SCAN) As the beta doesn’t support time display, I will ignore these bytes.

Links:  
* https://martinsuniverse.de/projekte/cdc_protokoll/cdc_protokoll.html
* https://www.mictronics.de/projects/cdc-protocols
* http://kovo-blog.blogspot.com/2014/02/cd-changer-emulator.html
* http://schuett.io/2013/09/avr-raspberry-pi-vw-beta-vag-cdc-faker/