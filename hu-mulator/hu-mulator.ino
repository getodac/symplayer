
#define hi(x) digitalWrite(x, HIGH)
#define lo(x) digitalWrite(x, LOW)

#define delayus(x) delayMicroseconds(x)

const int txpin = 13;

uint8_t buf[4];

void setup() {
  Serial.begin(9600);
  pinMode(txpin, OUTPUT);

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));

  Serial.println("Start transmitting...");
}

void loop() {
  uint8_t b[4] = {0x53, 0x2C, 0x05, 0xFA};
  uint8_t randNumber = random(255);
  b[2] = randNumber;
  b[3] = ~randNumber;
  delay(2000);
  txCmd(&b[0]);
}


// lsb first
void txCmd(uint8_t *b) {

  hi(txpin);
  delayus(9000);
  lo(txpin);
  delayus(4500);
  
  for(int i=0;i<4;i++) {
    for(int j=0;j<8;j++) {
      hi(txpin);
      delayus(550);
      lo(txpin);
      if (((b[i] >> j)  & 0x01) == 1) {
        delayus(1700);
      } else {
        delayus(550);
      }
    }
  }
  hi(txpin);
  delayus(550);
  lo(txpin);
  Serial.print(b[0], HEX);
  Serial.print(" ");
  Serial.print(b[1], HEX);
  Serial.print(" ");
  Serial.print(b[2], HEX);
  Serial.print(" ");
  Serial.print(b[3], HEX);
  Serial.println(" ");
}
