About
---------------
This project is intended for adding mp3/ogg playing support for Skoda Symphony Car Radio.

![Symphony Car Radio](assets/Skoda-Fabia-Symphony-CD-player.jpg)

This CD/Radio doesn't have AUX or other audio input type. The solution - build a CD Changer Emulator with mp3 support.

[Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/) emulates the CD Changer.

![Raspberry Pi Zero W](assets/rpi-zero-w.png)

Raspberry Pi Zero W headers (pinouts):

![Raspberry Pi Zero W headers](assets/rpi-gpio-40.png)

Connection between RPI and PCM5102:

    DAC BOARD   > Raspberry Pi Zero W connector J8
    -----------------------------------------------
    SCK         > PIN 9     (GND) (Internally generated)
    BCK         > PIN 12    (GPIO18)
    DIN         > PIN 40    (GPIO21)
    LRCK        > PIN 35    (GPIO19)
    GND         > PIN 6     (GND) Ground
    VIN         > PIN 2     (5V)


Connection between Car radion and CDC Emulator:

![SymPlayer high level diagram](assets/symplayer-high-level.png)

Symphony Car Radio connector:

![Symphony Car Radio connector](assets/Blaupunkt-gamma.gif)

Electronic Schematics:

![Electronic Schematics](assets/schematics.png)